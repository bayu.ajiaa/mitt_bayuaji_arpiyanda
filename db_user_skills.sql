-- create database user skills
CREATE DATABASE db_user_skills;

-- create table user
CREATE TABLE db_user_skills.dbo.tbl_m_user (
	username nvarchar(50) COLLATE Latin1_General_CI_AS NOT NULL,
	password nvarchar(50) COLLATE Latin1_General_CI_AS NOT NULL,
	CONSTRAINT tbl_m_user_PK PRIMARY KEY (username)
);

-- create table user_skill
CREATE TABLE db_user_skills.dbo.tbl_t_user_skills (
	user_skill_id nvarchar(50) NOT NULL,
	username_fk nvarchar(50) NOT NULL,
	skill_id_fk int NOT NULL,
	skill_level_id_fk int NOT NULL,
	CONSTRAINT tbl_t_user_skills_PK PRIMARY KEY (user_skill_id)
);

-- create table skill level
CREATE TABLE db_user_skills.dbo.tbl_m_skill_level (
	skill_level_id int IDENTITY (1,1) NOT NULL,
	skill_level_name varchar(500) NOT NULL,
	CONSTRAINT tbl_m_skill_level_PK PRIMARY KEY (skill_level_id)
);

-- create table skill
CREATE TABLE db_user_skills.dbo.tbl_m_skill (
	skill_id int IDENTITY (1,1) NOT NULL,
	skill_name varchar(500) NOT NULL,
	CONSTRAINT tbl_m_skill_PK PRIMARY KEY (skill_id)
);

-- create table user profile
CREATE TABLE db_user_skills.dbo.tbl_t_user_profile (
	username_fk nvarchar(50) NOT NULL,
	name varchar(50) NOT NULL,
	address nvarchar(500) NOT NULL,
	bod date NOT NULL,
	email nvarchar(50) NOT NULL,
	CONSTRAINT tbl_t_user_profile_PK PRIMARY KEY (username_fk)
);


