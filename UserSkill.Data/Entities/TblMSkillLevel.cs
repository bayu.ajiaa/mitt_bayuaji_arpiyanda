﻿using System;
using System.Collections.Generic;

#nullable disable

namespace UserSkill.Data.Entities
{
    public partial class TblMSkillLevel
    {
        public int SkillLevelId { get; set; }
        public string SkillLevelName { get; set; }
    }
}
