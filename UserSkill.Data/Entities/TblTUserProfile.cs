﻿using System;
using System.Collections.Generic;

#nullable disable

namespace UserSkill.Data.Entities
{
    public partial class TblTUserProfile
    {
        public string UsernameFk { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime Bod { get; set; }
        public string Email { get; set; }
    }
}
