﻿using System;
using System.Collections.Generic;

#nullable disable

namespace UserSkill.Data.Entities
{
    public partial class TblMSkill
    {
        public int SkillId { get; set; }
        public string SkillName { get; set; }
    }
}
