﻿using System;
using System.Collections.Generic;

#nullable disable

namespace UserSkill.Data.Entities
{
    public partial class TblMUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
