﻿using System;
using System.Collections.Generic;

#nullable disable

namespace UserSkill.Data.Entities
{
    public partial class TblTUserSkill
    {
        public string UserSkillId { get; set; }
        public string UsernameFk { get; set; }
        public int SkillIdFk { get; set; }
        public int SkillLevelIdFk { get; set; }
    }
}
