﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using UserSkill.Data.Entities;

#nullable disable

namespace UserSkill.Data.Context
{
    public partial class UserSkillsContext : DbContext
    {
        public UserSkillsContext()
        {
        }

        public UserSkillsContext(DbContextOptions<UserSkillsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblMSkill> TblMSkills { get; set; }
        public virtual DbSet<TblMSkillLevel> TblMSkillLevels { get; set; }
        public virtual DbSet<TblMUser> TblMUsers { get; set; }
        public virtual DbSet<TblTUserProfile> TblTUserProfiles { get; set; }
        public virtual DbSet<TblTUserSkill> TblTUserSkills { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=localhost; initial catalog=db_user_skills; user id=sa; password=iknowyou10; MultipleActiveResultSets=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<TblMSkill>(entity =>
            {
                entity.HasKey(e => e.SkillId)
                    .HasName("tbl_m_skill_PK");

                entity.ToTable("tbl_m_skill");

                entity.Property(e => e.SkillId).HasColumnName("skill_id");

                entity.Property(e => e.SkillName)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("skill_name");
            });

            modelBuilder.Entity<TblMSkillLevel>(entity =>
            {
                entity.HasKey(e => e.SkillLevelId)
                    .HasName("tbl_m_skill_level_PK");

                entity.ToTable("tbl_m_skill_level");

                entity.Property(e => e.SkillLevelId).HasColumnName("skill_level_id");

                entity.Property(e => e.SkillLevelName)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("skill_level_name");
            });

            modelBuilder.Entity<TblMUser>(entity =>
            {
                entity.HasKey(e => e.Username)
                    .HasName("tbl_m_user_PK");

                entity.ToTable("tbl_m_user");

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .HasColumnName("username");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("password");
            });

            modelBuilder.Entity<TblTUserProfile>(entity =>
            {
                entity.HasKey(e => e.UsernameFk)
                    .HasName("tbl_t_user_profile_PK");

                entity.ToTable("tbl_t_user_profile");

                entity.Property(e => e.UsernameFk)
                    .HasMaxLength(50)
                    .HasColumnName("username_fk");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasColumnName("address");

                entity.Property(e => e.Bod)
                    .HasColumnType("date")
                    .HasColumnName("bod");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("email");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<TblTUserSkill>(entity =>
            {
                entity.HasKey(e => e.UserSkillId)
                    .HasName("tbl_t_user_skills_PK");

                entity.ToTable("tbl_t_user_skills");

                entity.Property(e => e.UserSkillId)
                    .HasMaxLength(50)
                    .HasColumnName("user_skill_id");

                entity.Property(e => e.SkillIdFk).HasColumnName("skill_id_fk");

                entity.Property(e => e.SkillLevelIdFk).HasColumnName("skill_level_id_fk");

                entity.Property(e => e.UsernameFk)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("username_fk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
